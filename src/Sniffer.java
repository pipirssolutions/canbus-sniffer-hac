import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.*;
import java.util.List;

import com.fazecast.jSerialComm.*;

import javax.swing.*;
import javax.swing.text.DefaultCaret;

/**
 * Created by robinpipirs on 10/03/16.
 */
public class Sniffer {


    public static void main(String[] args) throws IOException {
        Collection<CanbusMessage> canBusMessagesEngineOn, canBusMessagesEngineOf;

        CanbusLogFileParser canbusLogfileParser = new CanbusLogFileParser();
        canBusMessagesEngineOn = canbusLogfileParser
                .ParseCanBusLog("/Users/robinpipirs/Desktop/lexus is220 canbusdata/lexus-engine-on-data-2016-03-11-17-30.asc");
        canBusMessagesEngineOf = canbusLogfileParser
                .ParseCanBusLog("/Users/robinpipirs/Desktop/lexus is220 canbusdata/lexus-engine-of-data-2016-03-11-17-27.asc");

        List<CanbusMessage> engineOnListFilteredWithEngineOf = new ArrayList<CanbusMessage>(canBusMessagesEngineOn);
        List<CanbusMessage> engineOfListFilteredWithEngineOn = new ArrayList<CanbusMessage>(canBusMessagesEngineOf);

        engineOnListFilteredWithEngineOf.removeAll(canBusMessagesEngineOf);
        engineOfListFilteredWithEngineOn.removeAll(canBusMessagesEngineOn);

        //filter list for repeats
        List<CanbusMessage> result = new ArrayList<CanbusMessage>();
        List<CanbusMessage> filtered = new ArrayList<CanbusMessage>();
        for (CanbusMessage canMsg : engineOnListFilteredWithEngineOf) {
            if (Collections.frequency(engineOnListFilteredWithEngineOf, canMsg) > 1) {
                result.add(canMsg);
            } else
                filtered.add(canMsg);
        }

        //filter list for repeats
        List<CanbusMessage> uniqueEngineOnIds = new ArrayList<>();
        for (CanbusMessage canMsg : result) {

            if (Collections.frequency(uniqueEngineOnIds, canMsg) < 1) {
                uniqueEngineOnIds.add(canMsg);
            }
        }


        /**
         * Make prints out of these
         */

        System.out.println("--> unique ids and data when engine on: " + uniqueEngineOnIds.size());

        System.out.println("Done parsing canbus messages: --> engine on codes: " + canBusMessagesEngineOn.size() + " engine of codes: " + canBusMessagesEngineOf.size());

        System.out.println("--> engine on filtered against of: " + engineOnListFilteredWithEngineOf.size());
        System.out.println("--> engine of filtered against on: " + engineOfListFilteredWithEngineOn.size());

        System.out.println("--> items that occur more than 1 time in engine on: " + result.size());
        System.out.println("--> unique ids and data in engine on: " + filtered.size());


        /** Creating filter lists etc **/

        List<CanbusMessage> engineOn = new ArrayList<>(canBusMessagesEngineOf);
        List<CanbusMessage> engineOf = new ArrayList<>(canBusMessagesEngineOn);

        System.out.println(engineOn.size());
        System.out.println(engineOf.size());

        engineOn.addAll(engineOf);

        System.out.println(engineOn.size());
        //filter list for repeats
        List<CanbusMessage> noRepeatsEngineOnOff = new ArrayList<>();

        int noofnoRep = 0;
        for (CanbusMessage can: engineOn
             ) {
            if (Collections.frequency(noRepeatsEngineOnOff, can) < 1){
                noRepeatsEngineOnOff.add(can);
                noofnoRep++;
            }
        }

        System.out.println("number of unique: "+noofnoRep);

        System.out.println("size of all codes: " + engineOn.size());

        System.out.println("Unique id repeats all code: " + noRepeatsEngineOnOff.size());

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "2C4", new String[]{"00", "00", "00", "15", "00", "00", "92", "75"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "260", new String[]{"0E", "00", "00", "00", "00", "00", "00", "78"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "22", new String[]{"01", "FC", "01", "FB", "00", "00", "00", "23"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "25", new String[]{"00", "0A", "00", "08", "82", "82", "82", "C5"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "25", new String[]{"00", "0A", "00", "08", "78", "78", "78", "A7"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "B4", new String[]{"00", "00", "00", "00", "00", "00", "00", "BC"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "2C1", new String[]{"08", "FC", "8A", "FC", "8A", "A4", "00", "83"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "2C1", new String[]{"08", "FC", "8A", "FC", "8A", "A4", "00", "83"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "452", new String[]{"43", "02", "00", "00", "00", "00", "00", "00"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "49A", new String[]{"00", "00", "00", "00", "00", "00", "00", "00"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "224", new String[]{"00", "00", "00", "00", "00", "00", "00", "00"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "2C6", new String[]{"00", "00", "00", "00", "00", "00", "00", "D0"}));

        noRepeatsEngineOnOff.add(new CanbusMessage("2000", "2C4", new String[]{"00", "00", "00", "17", "00", "00", "92", "77"}));

        System.out.println("size: "+ noRepeatsEngineOnOff.size());


        /** things happening in the gui here **/
        SerialCommunicationService scs = new SerialCommunicationService();
        CanbusDataView canBusView = new CanbusDataView();
        new LiveCommunicationObserver(scs, canBusView.getView());

        FilterManager filterManager = new FilterManager(canBusView.getFilterView());

        ListFilter listFilter = new ListFilter(noRepeatsEngineOnOff);
        filterManager.setFilter(listFilter);

        new FilterCommunicationObserver(scs,canBusView.getFilterView()).setFilterManager(filterManager);
        final Thread thread = new Thread(scs);
        thread.start();

        /** Gui is built here**/

        JFrame mainJFrame;
        mainJFrame = new JFrame();
        mainJFrame.setLayout(new BorderLayout());
        mainJFrame.setTitle("Pipirs Performance canbus sniffing software");
        mainJFrame.setBackground(Color.BLUE);

        mainJFrame.add(canBusView,BorderLayout.WEST);
        JPanel messageAndFilterPanel = new JPanel(new BorderLayout());
        messageAndFilterPanel.add(new SendMessageModel(scs),BorderLayout.NORTH);
        messageAndFilterPanel.add(new FilterListModel(listFilter),BorderLayout.SOUTH);
        mainJFrame.add(messageAndFilterPanel, BorderLayout.EAST);
        mainJFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainJFrame.pack();
        mainJFrame.setVisible(true);

        /** connecting ui components to the services**/


    }

    public static class SendMessageModel extends JPanel {

        private JButton sendBtn = new JButton("Send!");
        private JTextField id = new JTextField(10);
        private JTextField data = new JTextField(10);
        private SerialCommunicationService scs;

        public SendMessageModel(SerialCommunicationService scs){
            this.scs=scs;
            JPanel panel = new JPanel(new BorderLayout());
            panel.add(sendBtn,BorderLayout.EAST);
            panel.add(id,BorderLayout.WEST);
            panel.add(data,BorderLayout.CENTER);
            add(panel);
            sendBtn.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String canId = id.getText();
                    String canData = data.getText();
                    String id;
                    String id2;
                    if ((canId.length() > 2) || (canId.length() < 3) && canData.length() > 2){
                        if (canId.length()>2){
                            id = "0"+canId.substring(0,1);
                            id2= canId.substring(1,3);
                        }
                        else{
                            id = "00";
                            id2 = canId;
                        }
                        String delims = "[ ]";
                        String[] tokens = canData.split(delims);
                        String[] message = new String[tokens.length+3];

                        message[0] = id;
                        message[1] = id2;
                        message[message.length-1] = "AA";

                        for (int i = 0; i < tokens.length;i++){
                            message[i+2] = tokens[i];
                        }

                        byte[] writeBytes = new byte[message.length];

                        for(int i = 0; i < writeBytes.length; i++){
                            writeBytes[i] = (byte)(Integer.parseInt(message[i],16) & 0xff);
                        }
                        try {
                            scs.getOutPutStream().write(writeBytes);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }

                    }
                }
            });
        }

    }

    public static class CanbusDataView extends JPanel {

        private JTextArea jta;
        private JTextArea jtaFiltered;

        public JTextArea getView(){
            return jta;
        }
        public JTextArea getFilterView(){
          return jtaFiltered;
        }

        public CanbusDataView() {

            jta = new JTextArea(40, 40);
            DefaultCaret caret = (DefaultCaret) jta.getCaret();
            caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
            jta.append("Live canBus signal will appear here.\n");
            JScrollPane sp = new JScrollPane(jta,
                    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            JScrollBar bar = sp.getVerticalScrollBar();
            bar.setPreferredSize(new Dimension(10, 0));

            jtaFiltered = new JTextArea(40, 40);
            DefaultCaret caret2 = (DefaultCaret) jtaFiltered.getCaret();
            caret2.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
            jtaFiltered.append("Filtered canBusSignal will appear here.\n");
            JScrollPane spFiltered = new JScrollPane(jtaFiltered,
                    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            JScrollBar bar2 = spFiltered.getVerticalScrollBar();
            bar2.setPreferredSize(new Dimension(10, 0));

            JPanel jView = new JPanel(new BorderLayout());
            jView.add(sp, BorderLayout.WEST);
            jView.add(new JPanel(), BorderLayout.CENTER);
            jView.add(spFiltered, BorderLayout.EAST);

            add(jView);

        }
    }


    public static class FilterListModel extends JPanel {

        JList list;
        DefaultListModel model;
        CanbusLogFileParser canbusLogFileParser = new CanbusLogFileParser();
        private List<CanbusMessage> canList;
        private ListFilter listFilter;

        public FilterListModel(ListFilter listFilter) {
            this.listFilter = listFilter;
            setLayout(new BorderLayout());
            model = new DefaultListModel();
            list = new JList(model);
            JScrollPane pane = new JScrollPane(list);
            JButton addButton = new JButton("Add");
            JButton removeButton = new JButton("Remove");
            JTextField id = new JTextField(10);
            JTextField data = new JTextField(10);
            JPanel canbusPanel = new JPanel(new BorderLayout());
            canbusPanel.add(id,BorderLayout.WEST);
            canbusPanel.add(data,BorderLayout.EAST);

            for (CanbusMessage canMsg: this.listFilter.getList()
                 ) {
                model.addElement(canMsg.toListString());
            }

            addButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String canId = id.getText();
                    String canData = data.getText();
                    if ((canId.length() > 2) || (canId.length() < 3) && canData.length() > 2){
                        String canString = "T: 0 ID: " + canId + ", Data: " + canData;
                        System.out.println(canString);
                        CanbusMessage canbusMessage = canbusLogFileParser.ParseCanBusString(canString);
                        if (canbusMessage != null){
                            model.addElement(canbusMessage.toListString());
                            listFilter.addToList(canbusMessage);
                        }
                    }
                }
            });
            removeButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (model.getSize() > 0) {
                        String canString = model.getElementAt(list.getSelectedIndex()).toString();
                        canString = "T: 2000 " + canString;
                        CanbusMessage deleteMsg = canbusLogFileParser.ParseCanBusString(canString);

                        model.removeElementAt(list.getSelectedIndex());
                        listFilter.removeFromList(deleteMsg);
                    }
                }
            });

            JPanel jPanel = new JPanel(new BorderLayout());
            jPanel.add(pane, BorderLayout.NORTH);
            jPanel.add(canbusPanel,BorderLayout.WEST);
            jPanel.add(addButton, BorderLayout.CENTER);
            jPanel.add(removeButton, BorderLayout.EAST);
            add(jPanel,BorderLayout.SOUTH);
        }
    }
}


